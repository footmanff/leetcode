package com.footmanff.leetcode;

import org.junit.Assert;
import org.junit.Test;

import java.util.HashMap;
import java.util.Map;

/**
 * https://leetcode-cn.com/problems/first-missing-positive/
 *
 * @author footmanff on 2020/8/13.
 */
public class P41 {

    /**
     * 利用哈希表
     *
     * 但是应该不满足「时间复杂度应为O(n)，并且只能使用常数级别的额外空间」的要求
     */
    public int firstMissingPositive(int[] nums) {
        Map<Integer, Boolean> map = new HashMap<>(nums.length);
        for (int num : nums) {
            if (num > 0) {
                map.put(num, Boolean.TRUE);
            }
        }
        for (int i = 1; ; i++) {
            if (map.get(i) == null) {
                return i;
            }
        }
    }

    @Test
    public void t1() {
        int[] nums = {3, 4, -1, 1};
        int r = new P41().firstMissingPositive(nums);
        Assert.assertTrue(r == 2);
    }

    @Test
    public void t2() {
        int[] nums = {1, 2, 0};
        int r = new P41().firstMissingPositive(nums);
        Assert.assertTrue(r == 3);
    }

    @Test
    public void t3() {
        int[] nums = {7, 8, 9, 11, 12};
        int r = new P41().firstMissingPositive(nums);
        Assert.assertTrue(r == 1);
    }

}
