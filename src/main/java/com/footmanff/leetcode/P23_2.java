package com.footmanff.leetcode;

import java.util.Arrays;
import java.util.Comparator;

/**
 * https://leetcode.com/problems/merge-k-sorted-lists/
 *
 * @author footmanff on 2020/4/2.
 */
public class P23_2 {

    public ListNode mergeKLists(ListNode[] initList) {
        if (initList == null || initList.length == 0) {
            return null;
        }
        Arrays.sort(initList, Comparator.comparingInt(e -> {
            if (e == null) {
                return -1;
            } else {
                return e.val;
            }
        }));

        Array newArray = null;
        Array next = null;
        for (int i = initList.length - 1; i >= 0; i--) {
            if (initList[i] == null) {
                continue;
            }
            newArray = new Array(initList[i], next);
            next = newArray;
        }

        if (newArray == null) {
            return null;
        }

        ListNode result = null;
        ListNode tail = null;
        Array firstArray = newArray;

        for (; ; ) {
            // 维护结果链表
            if (result == null) {
                result = firstArray.firstNode;
                tail = result;
            } else {
                tail.next = firstArray.firstNode;
                tail = firstArray.firstNode;
            }

            // 首一个node list弹出一个node
            firstArray.firstNode = firstArray.firstNode.next;

            if (firstArray.firstNode == null) {
                // 如果首一个node list为空，并且也没有下一个node list，中断大循环，结束任务
                if (firstArray.nextList == null) {
                    break;
                }
                // 如果首一个node list为空，取下一次node list
                firstArray = firstArray.nextList;
                continue;
            }

            int i = firstArray.firstNode.val;

            if (firstArray.nextList == null) {
                // 如果node list只剩下一个，不再去执行插入排序
                continue;
            }

            // 遍历指正
            Array p = firstArray.nextList;
            // 遍历指正，位置-1
            Array p_1 = firstArray;

            boolean end = false;
            while (i > p.firstNode.val) {
                // 到末尾，终止
                p_1 = p;
                p = p.nextList;
                if (p == null) {
                    end = true;
                    break;
                }
            }

            if (p_1 == firstArray) {
                // 如果没有移动过，无需对新元素排序
                continue;
            } else if (end) {
                // 到达末尾
                p_1.nextList = firstArray;

                firstArray = firstArray.nextList;

                p_1.nextList.nextList = null;
            } else {
                Array tmp = firstArray.nextList;

                // 在链表中，按顺序插入
                p_1.nextList = firstArray;
                firstArray.nextList = p;

                // 如果移动过，取下一个Array
                firstArray = tmp;
            }


        }
        return result;
    }

    private static class Array {
        private ListNode firstNode;
        private Array nextList;

        public Array(ListNode firstNode, Array nextList) {
            this.firstNode = firstNode;
            this.nextList = nextList;
        }

        public ListNode getFirstNode() {
            return firstNode;
        }

        public Array setFirstNode(ListNode firstNode) {
            this.firstNode = firstNode;
            return this;
        }

        public Array getNextList() {
            return nextList;
        }

        public Array setNextList(Array nextList) {
            this.nextList = nextList;
            return this;
        }

        @Override
        public String toString() {
            if (firstNode != null) {
                return firstNode.toString();
            }
            return "empty node";
        }
    }

    private static class ListNode {
        int val;
        ListNode next;

        ListNode(int x) {
            val = x;
        }

        @Override
        public String toString() {
            StringBuilder info = new StringBuilder();
            info.append(val);
            ListNode next = this.next;
            while (next != null) {
                info.append("->");
                info.append(next.val);
                next = next.next;
            }
            return info.toString();
        }
    }

    /**
     * Input:
     * [
     * 1->4->5,
     * 1->3->4,
     * 2->6
     * ]
     * Output: 1->1->2->3->4->4->5->6
     *
     * 1->1->2->3->4->4->5->6
     */
    public static void main(String[] args) {
//        ListNode[] initList = new ListNode[3];
//        initList[0] = new ListNode(1);
//        initList[0].next = new ListNode(4);
//        initList[0].next.next = new ListNode(5);
//
//        initList[1] = new ListNode(1);
//        initList[1].next = new ListNode(3);
//        initList[1].next.next = new ListNode(4);
//
//        initList[2] = new ListNode(2);
//        initList[2].next = new ListNode(6);

//        ListNode[] initList = new ListNode[0];

        ListNode[] initList = new ListNode[2];
        initList[0] = new ListNode(1);
        initList[0].next = new ListNode(2);
        initList[0].next.next = new ListNode(2);

        initList[1] = new ListNode(1);
        initList[1].next = new ListNode(1);
        initList[1].next.next = new ListNode(2);

        ListNode node = new P23_2().mergeKLists(initList);
        System.out.println(node);
    }

}
