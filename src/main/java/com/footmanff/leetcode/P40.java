package com.footmanff.leetcode;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

/**
 * @author footmanff on 2020/8/14.
 */
public class P40 {

    public List<List<Integer>> combinationSum2(int[] cans, int target) {
        Arrays.sort(cans);
        List<List<Integer>> result = new ArrayList<>();
        LinkedList<Integer> stack = new LinkedList<>();

        for (int i = 0; i < cans.length && cans[i] <= target; i++) {
            if (i > 0 && cans[i] == cans[i - 1]) {
                continue;
            }
            recur(stack, result, cans, i, target);
        }

        return result;
    }

    private void recur(LinkedList<Integer> stack, List<List<Integer>> result, int[] cans, int posi, int target) {
        stack.addLast(cans[posi]);
        target = target - cans[posi];

        if (target == 0) {
            result.add(new ArrayList<>(stack));
            stack.removeLast();
            return;
        }
        if (posi + 1 <= cans.length - 1 && cans[posi + 1] > target) {
            stack.removeLast();
            return;
        }
        for (int i = posi + 1; i < cans.length && cans[i] <= target; i++) {
            if (i > posi + 1 && cans[i] == cans[i - 1]) {
                continue;
            }
            recur(stack, result, cans, i, target);
        }
        stack.removeLast();
    }

}
