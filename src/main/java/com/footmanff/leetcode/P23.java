package com.footmanff.leetcode;

/**
 * https://leetcode.com/problems/merge-k-sorted-lists/
 * 
 * @author footmanff on 2020/4/2.
 */
public class P23 {

    // 暴力
    public ListNode mergeKLists(ListNode[] initList) {

        int matchP = -1;

        ListNode result = null;
        ListNode tail = null;

        for (; ; ) {
            boolean allNull = true;
            int min = Integer.MAX_VALUE;
            for (int i = 0; i < initList.length; i++) {
                if (initList[i] == null) {
                    continue;
                }
                allNull = false;
                if (initList[i].val <= min) {
                    min = initList[i].val;
                    matchP = i;
                }
            }
            if (allNull) {
                break;
            }
            if (result == null) {
                result = initList[matchP];
                tail = result;
            } else {
                tail.next = initList[matchP];
                tail = initList[matchP];
            }
            initList[matchP] = initList[matchP].next;
        }

        return result;
    }

    private static class ListNode {
        int val;
        ListNode next;

        ListNode(int x) {
            val = x;
        }
    }

    public static void main(String[] args) {
        ListNode[] initList = new ListNode[2];
        initList[0] = new ListNode(1);
        initList[0].next = new ListNode(2);
        initList[1] = new ListNode(3);
        initList[1].next = new ListNode(4);
        
        new P23().mergeKLists(initList);
    }

}
