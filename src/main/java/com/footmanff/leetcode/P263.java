package com.footmanff.leetcode;

/**
 * https://leetcode-cn.com/problems/ugly-number/
 *
 * @author footmanff on 2020/8/11.
 */
public class P263 {

    /**
     * 暴力
     */
    public boolean isUgly(int num) {
        if (num < 1) {
            return false;
        }
        while (num % 2 == 0) {
            num = num / 2;
        }
        while (num % 3 == 0) {
            num = num / 3;
        }
        while (num % 5 == 0) {
            num = num / 5;
        }
        return num == 1;
    }

    public static void main(String[] args) {
        int n = 30;
        System.out.println(new P263().isUgly(n));
    }

}
