package com.footmanff.leetcode.stack;

import org.junit.Assert;
import org.junit.Test;

/**
 * https://leetcode-cn.com/problems/minimum-add-to-make-parentheses-valid/
 *
 * @author footmanff on 2020/9/10.
 */
public class P921 {

    public int minAddToMakeValid(String s) {
        int posi = 0;
        int retr = 0;
        for (int i = 0; i < s.length(); i++) {
            char c = s.charAt(i);
            if (c == '(') {
                posi++;
            } else {
                if (posi > 0) {
                    posi--;
                } else {
                    retr++;
                }
            }
        }
        return posi + retr;
    }

    @Test
    public void t1() {
        String s = "())";
        int r = 1;
        Assert.assertEquals(new P921().minAddToMakeValid(s), r);
    }

    @Test
    public void t2() {
        String s = "(((";
        int r = 3;
        Assert.assertEquals(new P921().minAddToMakeValid(s), r);
    }

    @Test
    public void t3() {
        String s = "()";
        int r = 0;
        Assert.assertEquals(new P921().minAddToMakeValid(s), r);
    }

    @Test
    public void t4() {
        String s = "()))((";
        int r = 4;
        Assert.assertEquals(new P921().minAddToMakeValid(s), r);
    }

    @Test
    public void t6() {
        String s = "(())";
        int r = 0;
        Assert.assertEquals(new P921().minAddToMakeValid(s), r);
    }

    @Test
    public void t7() {
        String s = "";
        int r = 0;
        Assert.assertEquals(new P921().minAddToMakeValid(s), r);
    }

    @Test
    public void t8() {
        String s = ")))(((";
        int r = 6;
        Assert.assertEquals(new P921().minAddToMakeValid(s), r);
    }
    
}
