package com.footmanff.leetcode;

import org.junit.Assert;
import org.junit.Test;

/**
 * https://leetcode-cn.com/problems/first-missing-positive/
 *
 * @author footmanff on 2020/8/13.
 */
public class P41_2 {

    /**
     * 复用nums数组，将每个数字维护到对应在数组中的位置
     * 
     * 输入: [3,4,-1,1]
     * 输出: 2
     */
    public int firstMissingPositive(int[] nums) {
        if (nums.length == 0) {
            return 1;
        }
        for (int i = 0; i < nums.length; ) {
            int n = nums[i];
            if (n <= 0) {
                i++;
                continue;
            }
            if (n > nums.length) {
                i++;
                continue;
            }
            int idx = i + 1;
            if (n == idx) {
                i++;
                continue;
            }
            if (n < idx) {
                nums[n - 1] = n;
                i++;
                continue;
            }
            int f = nums[n - 1];
            if (f == n) {
                nums[i] = 0;
                i ++;
                continue;
            }
            nums[n - 1] = n;
            nums[i] = f;
        }
        for (int i = 0; i < nums.length; i++) {
            if (nums[i] != i + 1) {
                return i + 1;
            }
        }
        return nums.length + 1;
    }

    @Test
    public void t1() {
        int[] nums = {3, 4, -1, 1};
        int r = new P41_2().firstMissingPositive(nums);
        Assert.assertEquals(r, 2);
    }

    @Test
    public void t2() {
        int[] nums = {1, 2, 0};
        int r = new P41_2().firstMissingPositive(nums);
        Assert.assertEquals(r, 3);
    }

    @Test
    public void t3() {
        int[] nums = {7, 8, 9, 11, 12};
        int r = new P41_2().firstMissingPositive(nums);
        Assert.assertEquals(r, 1);
    }

    @Test
    public void t4() {
        int[] nums = {};
        int r = new P41_2().firstMissingPositive(nums);
        Assert.assertEquals(r, 1);
    }

    @Test
    public void t5() {
        int[] nums = {1, 2, 3, 4};
        int r = new P41_2().firstMissingPositive(nums);
        Assert.assertEquals(r, 5);
    }

    @Test
    public void t6() {
        int[] nums = {2, 2, 2, 2};
        int r = new P41_2().firstMissingPositive(nums);
        Assert.assertEquals(r, 1);
    }
    
}