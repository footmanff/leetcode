package com.footmanff.leetcode;

import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import com.google.common.collect.Lists;

/**
 * https://leetcode-cn.com/problems/spiral-matrix/
 *
 * @author footmanff on 2020/8/12.
 */
public class P54 {

    public List<Integer> spiralOrder(int[][] matrix) {
        if (matrix.length == 0) {
            return Collections.emptyList();
        }

        // 宽
        int w = matrix[0].length;
        // 高
        int h = matrix.length;

        if (w == 0) {
            return Collections.emptyList();
        }

        List<Integer> result = new ArrayList<>(w * h);

        int r = 1;

        // 顶部边遍历区间
        int t1 = 0;
        int t2 = w - 2;

        // 右侧边遍历区间
        int r1 = 0;
        int r2 = h - 2;

        // 底部边遍历区间
        int b1 = w - 1;
        int b2 = 1;

        // 左侧边遍历区间
        int l1 = h - 1;
        int l2 = 1;

        while ((t2 - t1 > -2 && r2 - r1 > -2) && (t1 <= t2 || r1 <= r2)) {
            boolean top1 = t1 == t2 + 1;
            boolean right1 = r1 == r2 + 1;

            // 顶部边遍历
            for (int i = t1; i <= t2; i++) {
                result.add(matrix[r - 1][i]);
            }

            // 右侧边遍历
            for (int i = r1; i <= r2; i++) {
                result.add(matrix[i][w - r]);
            }

            // 底部边遍历
            for (int i = b1; i >= (right1 ? b1 : b2); i--) {
                result.add(matrix[h - r][i]);
            }

            // 左侧边遍历
            for (int i = l1; i >= (top1 ? l1 : l2); i--) {
                result.add(matrix[i][r - 1]);
            }

            t1++;
            t2--;
            r1++;
            r2--;
            b1--;
            b2++;
            l1--;
            l2++;

            r++;
        }

        if (t1 - t2 == 1 && r1 - r2 == 1) {
            result.add(matrix[t1][r1]);
        }

        return result;
    }

    @Test
    public void t1() {
        int[][] m = new int[][]{{1, 2, 3}, {4, 5, 6}, {7, 8, 9}};
        List<Integer> result = new P54().spiralOrder(m);
        Assert.assertEquals(result, Lists.newArrayList(1, 2, 3, 6, 9, 8, 7, 4, 5));
    }

    @Test
    public void t2() {
        int[][] m = new int[][]{{1, 2}, {3, 4}};
        List<Integer> result = new P54().spiralOrder(m);
        Assert.assertEquals(result, Lists.newArrayList(1, 2, 4, 3));
    }

    @Test
    public void t3() {
        int[][] m = new int[][]{{1}};
        List<Integer> result = new P54().spiralOrder(m);
        Assert.assertEquals(result, Lists.newArrayList(1));
    }

    @Test
    public void t4() {
        int[][] m = new int[][]{};
        List<Integer> result = new P54().spiralOrder(m);
        Assert.assertEquals(result, Lists.newArrayList());
    }

    @Test
    public void t5() {
        int[][] m = new int[][]{{}};
        List<Integer> result = new P54().spiralOrder(m);
        Assert.assertEquals(result, Lists.newArrayList());
    }

    @Test
    public void t6() {
        int[][] m = new int[][]{{1, 2, 3, 4}, {5, 6, 7, 8}, {9, 10, 11, 12}};
        List<Integer> result = new P54().spiralOrder(m);
        Assert.assertEquals(result, Lists.newArrayList(1, 2, 3, 4, 8, 12, 11, 10, 9, 5, 6, 7));
    }

    @Test
    public void t7() {
        int[][] m = new int[][]{{0, 6, 9}};
        List<Integer> result = new P54().spiralOrder(m);
        Assert.assertEquals(result, Lists.newArrayList(0, 6, 9));
    }

    @Test
    public void t8() {
        int[][] m = new int[][]{{0}, {6}, {9}};
        List<Integer> result = new P54().spiralOrder(m);
        Assert.assertEquals(result, Lists.newArrayList(0, 6, 9));
    }

    @Test
    public void t9() {
        int[][] m = new int[][]{{1, 2, 3, 4, 5, 6, 7, 8, 9, 10}};
        List<Integer> result = new P54().spiralOrder(m);
        Assert.assertEquals(result, Lists.newArrayList(1, 2, 3, 4, 5, 6, 7, 8, 9, 10));
    }

    @Test
    public void t10() {
        int[][] m = new int[][]{{1, 2, 3, 4, 5, 6, 7, 8, 9, 10}, {11, 12, 13, 14, 15, 16, 17, 18, 19, 20}};
        List<Integer> result = new P54().spiralOrder(m);
        Assert.assertEquals(result, Lists.newArrayList(1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 20, 19, 18, 17, 16, 15, 14, 13, 12, 11));
    }

}
