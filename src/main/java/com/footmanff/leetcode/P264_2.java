package com.footmanff.leetcode;

import java.util.HashSet;
import java.util.PriorityQueue;
import java.util.Set;

/**
 * @author footmanff on 2020/8/12.
 */
public class P264_2 {

    /**
     * 小顶堆
     */
    public int nthUglyNumber(int n) {
        if (n == 1) {
            return 1;
        }
        Set<Long> set = new HashSet<>();
        PriorityQueue<Long> queue = new PriorityQueue<>(n * 2);
        queue.add(1L);
        set.add(1L);
        while (n > 1) {
            Long min = queue.poll();
            set.remove(min);

            Long n1 = min * 2;
            Long n2 = min * 3;
            Long n3 = min * 5;
            
            if (!set.contains(n1)) {
                queue.add(n1);
                set.add(n1);
            }

            if (!set.contains(n2)) {
                queue.add(n2);
                set.add(n2);
            }

            if (!set.contains(n3)) {
                queue.add(n3);
                set.add(n3);
            }
            
            n--;
        }
        return queue.poll().intValue();
    }

    public static void main(String[] args) {
        int n = 1690; // 2123366400
        System.out.println(new P264_2().nthUglyNumber(n));

        for (int i = 1; i < 30; i++) {
            System.out.print(new P264_2().nthUglyNumber(i) + " ");
        }
    }

}