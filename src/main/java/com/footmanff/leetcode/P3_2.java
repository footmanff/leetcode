package com.footmanff.leetcode;

import java.util.HashMap;
import java.util.Map;

/**
 * https://leetcode.com/problems/longest-substring-without-repeating-characters/
 *
 * @author footmanff on 2020/3/30.
 */
public class P3_2 {

    /**
     * 滑动窗口
     */
    public int lengthOfLongestSubstring(String str) {
        if (str == null || str.length() == 0) {
            return 0;
        }
        int length = str.length();

        Map<Character, Integer> cpMap = new HashMap<>();

        int from = 0;
        int to = 0;

        int max = 0;

        for (; to <= length - 1; ) {
            Character c = str.charAt(to);

            Integer exist = cpMap.putIfAbsent(c, to);

            // 不存在
            if (exist == null) {
                if (to == length - 1) {
                    if (cpMap.size() > max) {
                        max = cpMap.size();
                        break;
                    }
                }
                to++;
                continue;
            }

            // 发生重复
            if (cpMap.size() > max) {
                max = cpMap.size();
            }

            for (int i = from; i <= exist; i++) {
                cpMap.remove(str.charAt(i));
            }

            from = exist + 1;
        }

        return max;
    }

    public static void main(String[] args) {
        P3_2 p1 = new P3_2();
        int a = p1.lengthOfLongestSubstring("pwwkew");
        System.out.println(a);
    }

}
