package com.footmanff.leetcode;

/**
 * https://leetcode.com/problems/string-to-integer-atoi/submissions/
 * 
 * @author footmanff on 2020/4/1.
 */
public class P8 {

    public int myAtoi(String str) {
        int first = 1;

        boolean signMatched = false;
        boolean numMatched = false;

        long r = 0;
        for (int i = 0; i < str.length(); i++) {
            char ch = str.charAt(i);

            if (ch == ' ') {
                if (!signMatched) {
                    continue;
                }
                if (!numMatched) {
                    break;
                }
                break;
            }

            if (ch == '-' || ch == '+') {
                if (signMatched) {
                    break;
                }
                if (numMatched) {
                    break;
                }
                signMatched = true;

                first = ch == '-' ? -1 : 1;
                continue;
            }

            if (ch >= '0' && ch <= '9') {
                if (!signMatched) {
                    signMatched = true;
                }
                numMatched = true;
                long newInt = r * 10 + toInt(ch);
                if (first == 1 && newInt > 2147483647L) {
                    return 2147483647;
                }
                if (first == -1 && newInt > 2147483648L) {
                    return -2147483648;
                }
                r = newInt;
                continue;
            }
            
            break;
        }
        return (int)r * first;
    }

    private int toInt(char ch) {
        return ch - 48;
    }

    public static void main(String[] args) {
        String[] str = new String[]{"-91283472332"};
        for (String s : str) {
            System.out.println(s + "   " + new P8().myAtoi(s));
        }
    }

}
