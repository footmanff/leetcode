package com.footmanff.leetcode;

/**
 * 暴力
 * 
 * @author footmanff on 2020/8/12.
 */
public class P264 {

    public int nthUglyNumber(int n) {
        if (n == 1) {
            return 1;
        }
        int p = 2;
        int i = 2;
        for(;;) {
            if (isUgly(i)) {
                if (p == n) {
                    return i;
                }
                p ++;
            }
            i ++;
        }
    }

    private boolean isUgly(int num) {
        if (num < 1) {
            return false;
        }
        while (num % 2 == 0) {
            num = num / 2;
        }
        while (num % 3 == 0) {
            num = num / 3;
        }
        while (num % 5 == 0) {
            num = num / 5;
        }
        return num == 1;
    }

    public static void main(String[] args) {
        int n = 1690;
        System.out.println(new P264().nthUglyNumber(n));
    }
    
}