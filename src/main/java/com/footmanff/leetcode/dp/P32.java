package com.footmanff.leetcode.dp;

import org.junit.Assert;
import org.junit.Test;

/**
 * https://leetcode-cn.com/problems/longest-valid-parentheses/
 *
 * @author footmanff on 2020/9/2.
 */
public class P32 {

    /*
        O(n) 直接模拟
        
        输入: ")()())"
        输出: 4
        解释: 最长有效括号子串为 "()()"
        
        (()(()()))
        
        (()()
         
     */
    public int longestValidParentheses(String s) {
        if (s.length() <= 1) {
            return 0;
        }
        int max = 0;
        int[] dp = new int[s.length()];
        if (s.charAt(0) == '(' && s.charAt(1) == ')') {
            dp[1] = 2;
            max = 2;
        }
        for (int i = 2; i < s.length(); i++) {
            char c = s.charAt(i);
            if (c == '(') {
                dp[i] = 0;
                continue;
            }
            if (s.charAt(i - 1) == '(') {
                dp[i] = dp[i - 2] + 2;
            } else {
                // ')'
                if (dp[i - 1] == 0) {
                    dp[i] = 0;
                } else {
                    if (dp[i - 1] == i) {
                        dp[i] = 0;
                    } else if (s.charAt(i - dp[i - 1] - 1) == '(') {
                        dp[i] = dp[i - 1] + 2;
                        if (i - dp[i - 1] - 1 > 0) {
                            dp[i] += dp[i - dp[i - 1] - 2];
                        }
                    } else {
                        dp[i] = 0;
                    }
                }
            }
            max = Math.max(max, dp[i]);
        }
        return max;
    }

    @Test
    public void t01() {
        String s = "((((((((";
        Assert.assertEquals(new P32().longestValidParentheses(s), 0);
    }

    @Test
    public void t02() {
        String s = "))))))))";
        Assert.assertEquals(new P32().longestValidParentheses(s), 0);
    }

    @Test
    public void t0() {
        String s = "()(())";
        Assert.assertEquals(new P32().longestValidParentheses(s), 6);
    }

    @Test
    public void t1() {
        String s = ")()())";
        Assert.assertEquals(new P32().longestValidParentheses(s), 4);
    }

    @Test
    public void t2() {
        String s = "()()";
        Assert.assertEquals(new P32().longestValidParentheses(s), 4);
    }

    @Test
    public void t3() {
        String s = "()(()()";
        Assert.assertEquals(new P32().longestValidParentheses(s), 4);
    }

    @Test
    public void t4() {
        String s = "(()";
        Assert.assertEquals(new P32().longestValidParentheses(s), 2);
    }

    @Test
    public void t5() {
        String s = ")(";
        Assert.assertEquals(new P32().longestValidParentheses(s), 0);
    }

    @Test
    public void t6() {
        String s = "()";
        Assert.assertEquals(new P32().longestValidParentheses(s), 2);
    }

    @Test
    public void t7() {
        String s = "()())";
        Assert.assertEquals(new P32().longestValidParentheses(s), 4);
    }

    @Test
    public void t8() {
        String s = "(";
        Assert.assertEquals(new P32().longestValidParentheses(s), 0);
    }

    @Test
    public void t9() {
        String s = ")";
        Assert.assertEquals(new P32().longestValidParentheses(s), 0);
    }

    @Test
    public void t10() {
        String s = "(())";
        Assert.assertEquals(new P32().longestValidParentheses(s), 4);
    }

    @Test
    public void t11() {
        String s = "()((((((((()()";
        Assert.assertEquals(new P32().longestValidParentheses(s), 4);
    }

    @Test
    public void t12() {
        String s = "()(())";
        Assert.assertEquals(new P32().longestValidParentheses(s), 6);
    }

    @Test
    public void t13() {
        String s = "()(()";
        Assert.assertEquals(new P32().longestValidParentheses(s), 2);
    }

}
