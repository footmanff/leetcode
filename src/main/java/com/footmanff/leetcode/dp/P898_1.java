package com.footmanff.leetcode.dp;

import com.footmanff.leetcode.util.Numbers;
import org.junit.Assert;
import org.junit.Test;

import java.util.HashSet;

/**
 * https://leetcode-cn.com/problems/bitwise-ors-of-subarrays/
 *
 * @author footmanff on 2020/9/10.
 */
public class P898_1 {

    /*
        按位或运算，再怎么算也不会有很多个数
        22%，515ms，不是很好        
     */
    public int subarrayBitwiseORs(int[] nums) {
        if (nums.length == 0) {
            return 0;
        }
        if (nums.length == 1) {
            return 1;
        }
        HashSet<Integer> last = new HashSet<>();
        last.add(nums[nums.length - 1]);

        HashSet<Integer> result = new HashSet<>(32);
        result.add(nums[nums.length - 1]);

        for (int i = nums.length - 2; i >= 0; i--) {
            HashSet<Integer> tmp = new HashSet<>(last.size());
            for (Integer n : last) {
                int t = n | nums[i];
                tmp.add(t);
                result.add(t);
            }
            tmp.add(nums[i]);
            result.add(nums[i]);
            last = tmp;
        }

        return result.size();
    }

    @Test
    public void t1() {
        int[] a = {1, 1, 2};
        Assert.assertEquals(3, new P898_1().subarrayBitwiseORs(a));
    }

    @Test
    public void t2() {
        int[] a = {0};
        Assert.assertEquals(1, new P898_1().subarrayBitwiseORs(a));
    }

    @Test
    public void t3() {
        int[] a = {1, 2, 4};
        Assert.assertEquals(6, new P898_1().subarrayBitwiseORs(a));
    }

    @Test
    public void t5() {
        int[] a = {1, 2, 4};
        Assert.assertEquals(6, new P898_1().subarrayBitwiseORs(a));
    }

    @Test
    public void t6() {
        int[] a = Numbers.geneIntList(5000, 10000);
        long s = System.currentTimeMillis();
        try {
            new P898_1().subarrayBitwiseORs(a);
        } finally {
            System.out.println(System.currentTimeMillis() - s);
        }
    }

}
