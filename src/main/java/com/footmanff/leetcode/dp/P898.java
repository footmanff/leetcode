package com.footmanff.leetcode.dp;

import com.footmanff.leetcode.util.Numbers;
import org.junit.Assert;
import org.junit.Test;

import java.util.HashSet;
import java.util.Set;

/**
 * https://leetcode-cn.com/problems/bitwise-ors-of-subarrays/
 * 
 * @author footmanff on 2020/9/10.
 */
public class P898 {

    /*
       暴力，不行
     */
    public int subarrayBitwiseORs(int[] nums) {
        if (nums.length == 0) {
            return 0;
        }
        if (nums.length == 1) {
            return 1;
        }
        int[][] dp = new int[nums.length][nums.length];

        Set<Integer> set = new HashSet<>();

        for (int i = 0; i < nums.length; i++) {
            dp[i][i] = nums[i];
            set.add(dp[i][i]);
            for (int j = i + 1; j < nums.length; j++) {
                dp[i][j] = dp[i][j - 1] | nums[j];
                set.add(dp[i][j]);
            }
        }
        return set.size();
    }

    @Test
    public void t1() {
        int[] a = {1, 1, 2};
        Assert.assertEquals(3, new P898().subarrayBitwiseORs(a));
    }

    @Test
    public void t2() {
        int[] a = {0};
        Assert.assertEquals(1, new P898().subarrayBitwiseORs(a));
    }

    @Test
    public void t3() {
        int[] a = {1, 2, 4};
        Assert.assertEquals(6, new P898().subarrayBitwiseORs(a));
    }

    @Test
    public void t5() {
        int[] a = {1, 2, 4};
        Assert.assertEquals(6, new P898().subarrayBitwiseORs(a));
    }

    @Test
    public void t6() {
        int[] a = Numbers.geneIntList(100, 100);
        Assert.assertEquals(6, new P898().subarrayBitwiseORs(a));
    }

}
