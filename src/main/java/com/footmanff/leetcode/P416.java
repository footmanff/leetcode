package com.footmanff.leetcode;

import org.junit.Assert;
import org.junit.Test;

/**
 * https://leetcode-cn.com/problems/partition-equal-subset-sum/
 *
 * @author footmanff on 2020/8/14.
 */
public class P416 {

    /**
     * 动态规划
     */
    public boolean canPartition(int[] nums) {
        int sum = 0;
        for (int num : nums) {
            sum += num;
        }
        if (sum % 2 != 0) {
            return false;
        }
        int half = sum / 2;

        int[] dp = new int[sum + 1];
        dp[0] = 1;
        for (int num : nums) {
            for (int i = sum; i >= num; i--) {
                if (dp[i - num] == 1) {
                    dp[i] = 1;
                    if (i == half) {
                        return true;
                    }
                }
            }
        }
        return false;
    }

    @Test
    public void t1() {
        int[] nums = {1, 5, 11, 5};
        Assert.assertTrue(new P416().canPartition(nums));
    }

    @Test
    public void t2() {
        int[] nums = {1, 2, 3, 5};
        Assert.assertFalse(new P416().canPartition(nums));
    }

    @Test
    public void t3() {
        int[] nums = {1, 5, 5, 11};
        Assert.assertTrue(new P416().canPartition(nums));
    }

    @Test
    public void t4() {
        int[] nums = {23, 13, 11, 7, 6, 5, 5};
        Assert.assertTrue(new P416().canPartition(nums));
    }

}
