package com.footmanff.leetcode;

import com.google.common.collect.Lists;
import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

/**
 * 递归
 *
 * @author footmanff on 2020/8/13.
 */
public class P39_1 {

    public List<List<Integer>> combinationSum(int[] cans, int target) {
        Arrays.sort(cans);
        List<List<Integer>> result = new ArrayList<>();
        LinkedList<Integer> stack = new LinkedList<>();

        for (int i = 0; i < cans.length && cans[i] <= target; i++) {
            recur(stack, result, cans, i, target);
        }

        return result;
    }

    /*
     * 输入：candidates = [2,3,6,7], target = 7,
     * 所求解集为：[7], [2,2,3]
     */
    private void recur(LinkedList<Integer> stack, List<List<Integer>> result, int[] cans, int posi, int target) {
        stack.addLast(cans[posi]);
        target = target - cans[posi];

        if (target == 0) {
            result.add(new ArrayList<>(stack));
            stack.removeLast();
            return;
        }
        if (cans[posi] > target) {
            stack.removeLast();
            return;
        }
        for (int i = posi; i < cans.length && cans[i] <= target; i++) {
            recur(stack, result, cans, i, target);
        }
        stack.removeLast();
    }

    @Test
    public void t1() {
        int[] c = {2, 3, 5};
        int target = 8;
        Assert.assertEquals(new P39_1().combinationSum(c, target), Lists.newArrayList(Lists.newArrayList(2, 2, 2, 2), Lists.newArrayList(2, 3, 3), Lists.newArrayList(3, 5)));
    }

    @Test
    public void t2() {
        int[] c = {2, 3, 6, 7};
        int target = 7;
        Assert.assertEquals(new P39_1().combinationSum(c, target), Lists.newArrayList(Lists.newArrayList(2, 2, 3), Lists.newArrayList(7)));
    }

}
