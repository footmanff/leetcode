package com.footmanff.leetcode;

import java.util.HashSet;
import java.util.PriorityQueue;
import java.util.Set;

/**
 * @author footmanff on 2020/8/12.
 */
public class P264_3 {

    /**
     * 动态规划
     */
    public int nthUglyNumber(int n) {
        if (n == 1) {
            return 1;
        }
        int b1 = 0;
        int b3 = 0;
        int b5 = 0;

        int[] list = new int[n];
        int p = 1;
        list[0] = 1;

        while (p <= n - 1) {
            int u1 = list[b1] * 2;
            int u3 = list[b3] * 3;
            int u5 = list[b5] * 5;

            int min = Math.min(Math.min(u1, u3), u5);
            if (min == u1) {
                b1++;
            }
            if (min == u3) {
                b3++;
            }
            if (min == u5) {
                b5++;
            }
            list[p] = min;
            p++;
        }
        return list[n - 1];
    }
    
    public static void main(String[] args) {
        int n = 1690; // 2123366400
        System.out.println(new P264_3().nthUglyNumber(n));
    }

}