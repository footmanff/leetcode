package com.footmanff.leetcode;

import org.junit.Test;

/**
 * https://leetcode-cn.com/problems/insertion-sort-list/submissions/
 * 
 * @author footmanff on 2020/8/14.
 */
public class P147 {

    /*
     * 插入排序
     * 
     * 执行用时：23 ms, 在所有 Java 提交中击败了36.63%的用户
     * 内存消耗：39.7 MB, 在所有 Java 提交中击败了27.27%的用户
     */
    public ListNode insertionSortList(ListNode head) {
        ListNode result = null;

        while (head != null) {
            ListNode tmp = head;
            head = head.next;
            tmp.next = null;

            if (result == null) {
                result = tmp;
                continue;
            }

            ListNode index = result;
            ListNode last = result;

            while (index != null && tmp.val > index.val) {
                last = index;
                index = index.next;
            }

            if (last == index) {
                tmp.next = index;
                result = tmp;
            } else if (index == null) {
                last.next = tmp;
            } else {
                last.next = tmp;
                tmp.next = index;
            }
        }
        return result;
    }

    public static class ListNode {
        int val;
        ListNode next;

        ListNode(int x) {
            val = x;
        }

        @Override
        public String toString() {
            StringBuilder info = new StringBuilder();
            info.append(val);
            ListNode n = this.next;
            while (n != null) {
                info.append(" ");
                info.append(n.val);
                n = n.next;
            }
            return info.toString();
        }
    }

    @Test
    public void t1() {
        // 4->2->1->3
        ListNode node = new ListNode(4);
        node.next = new ListNode(2);
        node.next.next = new ListNode(1);
        node.next.next.next = new ListNode(3);
        System.out.println(insertionSortList(node));
    }

    @Test
    public void t2() {
        // 4
        ListNode node = new ListNode(4);
        System.out.println(insertionSortList(node));
    }

    @Test
    public void t3() {
        // 1 2
        ListNode node = new ListNode(1);
        node.next = new ListNode(2);
        System.out.println(insertionSortList(node));
    }

}