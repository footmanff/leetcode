package com.footmanff.leetcode.greedy;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * https://leetcode-cn.com/problems/jump-game-ii/
 *
 * @author footmanff on 2020/9/9.
 */
public class P45_1 {

    public int jump(int[] nums) {
        if (nums.length == 0 || nums.length == 1) {
            return 0;
        }
        int[] mem = new int[nums.length];
        int base = 0;
        for (int i = 0; i < nums.length; i++) {
            for (int j = base + 1; j < nums.length && j <= i + nums[i]; j++) {
                mem[j] = mem[i] + 1;
                base = j;
            }
        }
        return mem[nums.length - 1];
    }

    @Test
    public void t0() {
    }

    @Test
    public void t1() {
        int[] nums = {2, 3, 1, 1, 4};
        int r = 2;
        assertEquals(r, new P45_1().jump(nums));
    }

    @Test
    public void t2() {
        int[] nums = {1, 1, 1, 1, 1};
        int r = 4;
        assertEquals(r, new P45_1().jump(nums));
    }

    @Test
    public void t3() {
        int[] nums = {1, 1, 2, 1, 1};
        int r = 3;
        assertEquals(r, new P45_1().jump(nums));
    }

    @Test
    public void t4() {
        int[] nums = {4, 2, 7, 2, 1, 3};
        int r = 2;
        assertEquals(r, new P45_1().jump(nums));
    }

    @Test
    public void t5() {
        int[] nums = {0};
        int r = 0;
        assertEquals(r, new P45_1().jump(nums));
    }

    @Test
    public void t6() {
        int[] nums = {5, 6, 4, 4, 6, 9, 4, 4, 7, 4, 4, 8, 2, 6, 8, 1, 5, 9, 6, 5, 2, 7, 9, 7, 9, 6, 9, 4, 1, 6, 8, 8, 4, 4, 2, 0, 3, 8, 5};
        int r = 5;
        assertEquals(r, new P45_1().jump(nums));
    }

    @Test
    public void t7() {
        int[] nums = {8, 2, 4, 4, 4, 9, 5, 2, 5, 8, 8, 0, 8, 6, 9, 1, 1, 6, 3, 5, 1, 2, 6, 6, 0, 4, 8, 6, 0, 3, 2, 8, 7, 6, 5, 1, 7, 0, 3, 4, 8, 3, 5, 9, 0, 4, 0, 1, 0, 5, 9, 2, 0, 7, 0, 2, 1, 0, 8, 2, 5, 1, 2, 3, 9, 7, 4, 7, 0, 0, 1, 8, 5, 6, 7, 5, 1, 9, 9, 3, 5, 0, 7, 5};
        int r = 13;
        assertEquals(r, new P45_1().jump(nums));
    }

    @Test
    public void t8() {
        int[] nums = {8, 2, 4, 4, 4, 9, 5, 2, 5, 8, 8, 0, 8, 6, 9, 1, 1, 6, 3, 5, 1, 2, 6, 6, 0, 4, 8, 6, 0, 3, 2, 8, 7, 6, 5, 1, 7, 0, 3, 4, 8, 3, 5, 9, 0, 4, 0, 1, 0, 5, 9, 2, 0, 7, 0, 2, 1, 0, 8, 2, 5, 1, 2, 3, 9, 7, 4, 7, 0, 0, 1, 8, 5, 6, 7, 5, 1, 9, 9, 3, 5, 0, 7, 5, 8, 2, 4, 4, 4, 9, 5, 2, 5, 8, 8, 0, 8, 6, 9, 1, 1, 6, 3, 5, 1, 2, 6, 6, 0, 4, 8, 6, 0, 3, 2, 8, 7, 6, 5, 1, 7, 0, 3, 4, 8, 3, 5, 9, 0, 4, 0, 1, 0, 5, 9, 2, 0, 7, 0, 2, 1, 0, 8, 2, 5, 1, 2, 3, 9, 7, 4, 7, 0, 0, 1, 8, 5, 6, 7, 5, 1, 9, 9, 3, 5, 0, 7, 5, 8, 2, 4, 4, 4, 9, 5, 2, 5, 8, 8, 0, 8, 6, 9, 1, 1, 6, 3, 5, 1, 2, 6, 6, 0, 4, 8, 6, 0, 3, 2, 8, 7, 6, 5, 1, 7, 0, 3, 4, 8, 3, 5, 9, 0, 4, 0, 1, 0, 5, 9, 2, 0, 7, 0, 2, 1, 0, 8, 2, 5, 1, 2, 3, 9, 7, 4, 7, 0, 0, 1, 8, 5, 6, 7, 5, 1, 9, 9, 3, 5, 0, 7, 5, 8, 2, 4, 4, 4, 9, 5, 2, 5, 8, 8, 0, 8, 6, 9, 1, 1, 6, 3, 5, 1, 2, 6, 6, 0, 4, 8, 6, 0, 3, 2, 8, 7, 6, 5, 1, 7, 0, 3, 4, 8, 3, 5, 9, 0, 4, 0, 1, 0, 5, 9, 2, 0, 7, 0, 2, 1, 0, 8, 2, 5, 1, 2, 3, 9, 7, 4, 7, 0, 0, 1, 8, 5, 6, 7, 5, 1, 9, 9, 3, 5, 0, 7, 5};
        int r = 52;
        assertEquals(r, new P45_1().jump(nums));
    }

}
