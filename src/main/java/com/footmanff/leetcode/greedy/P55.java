package com.footmanff.leetcode.greedy;

import org.junit.Assert;
import org.junit.Test;

import java.util.Random;

/**
 * https://leetcode-cn.com/problems/jump-game/
 *
 * @author footmanff on 2020/8/14.
 */
public class P55 {

    /*
       描述中有一行：数组中的每个元素代表你在该位置可以跳跃的最大长度。
       是最大长度，不是固定只能跳那个长度
     */
    public boolean canJump(int[] nums) {
        if (nums.length == 0) {
            return false;
        }
        if (nums.length == 1) {
            return true;
        }
        int max = 0;
        for (int i = 0; i < nums.length; i++) {
            if (i + nums[i] > max) {
                max = nums[i] + i;
                if (max >= nums.length - 1) {
                    return true;
                }
            }
            if (nums[i] == 0 && i == max) {
                break;
            }
        }
        return max >= nums.length - 1;
    }

    @Test
    public void t1() {
        int[] nums = {2, 3, 1, 1, 4};
        Assert.assertEquals(new P55().canJump(nums), true);
    }

    @Test
    public void t2() {
        int[] nums = {3, 2, 1, 0, 4};
        Assert.assertEquals(new P55().canJump(nums), false);
    }

    @Test
    public void t3() {
        int[] nums = {1};
        Assert.assertEquals(new P55().canJump(nums), true);
    }

    @Test
    public void t4() {
        int[] nums = {0};
        Assert.assertEquals(new P55().canJump(nums), true);
    }

    @Test
    public void t5() {
        int[] nums = {1, 1, 1, 1};
        Assert.assertEquals(new P55().canJump(nums), true);
    }

    @Test
    public void t6() {
        int[] nums = {2, 0};
        Assert.assertEquals(new P55().canJump(nums), true);
    }

    @Test
    public void t7() {
        int[] nums = {0, 0, 0};
        Assert.assertEquals(new P55().canJump(nums), false);
    }

    @Test
    public void t8() {
        int[] nums = {1, 0, 0};
        Assert.assertEquals(new P55().canJump(nums), false);
    }

    @Test
    public void t9() {
        int[] nums = {3, 2, 4, 5, 3, 6, 4, 2, 12, 4, 12, 9, 2};
        Assert.assertEquals(new P55().canJump(nums), true);
    }

    @Test
    public void t10() {
        Random r = new Random();
        int[] nums = new int[100];
        for (int i = 0; i < 10000; i++) {
            for (int j = 0; j < 100; j++) {
                nums[j] = r.nextInt(30);
                if (nums[j] == 0) {
                    nums[j]++;
                }
            }
            Assert.assertEquals(new P55().canJump(nums), true);
        }
    }

}
