package com.footmanff.leetcode.greedy;

import org.junit.Test;

import java.util.LinkedList;

import static org.junit.Assert.assertEquals;

/**
 * https://leetcode-cn.com/problems/jump-game-ii/
 *
 * @author footmanff on 2020/9/9.
 */
public class P45 {

    /**
     * 模拟跳跃动作，每次跳跃尽量多跳格子
     * 
     * 超时，有很多跳跃动作重复
     * 
     * 做了一些优化，依然不行
     */
    public int jump(int[] nums) {
        if (nums.length == 0 || nums.length == 1) {
            return 0;
        }
        LinkedList<Frame> stack = new LinkedList<>();
        
        int min = nums.length;

        stack.addLast(new Frame(0, nums[0]));

        while (true) {
            Frame last = stack.peekLast();
            if (last == null) {
                break;
            }
            if (last.p == nums.length - 1) {
                min = Math.min(min, stack.size());

                // printStack(stack, nums);
                // System.out.println(min);
                
                stack.pollLast();
                continue;
            }

            if (last.len <= 0) {
                stack.pollLast();
                continue;
            }

            last.len = Math.min(last.len, nums.length - 1 - last.p);
            int nextP = last.p + last.len;
            
            if (stack.size() + 1 == min) {
                stack.pollLast();
                continue;
            }
            
            if (stack.size() + 2 == min && nextP < nums.length - 1) {
                stack.pollLast();
                continue;
            }
            
            last.len--;

            int nextLen = nums[nextP];

            stack.addLast(new Frame(nextP, nextLen));
        }
        
        return min - 1;
    }

    static class Frame {
        int p;
        int len;

        public Frame(int p, int len) {
            this.p = p;
            this.len = len;
        }

        @Override
        public String toString() {
            return "p: " + p + " len: " + len;
        }
    }

    private void printStack(LinkedList<Frame> stack, int[] nums) {
        StringBuilder info = new StringBuilder();
        for (Frame frame : stack) {
            info.append(nums[frame.p]);
            info.append("_");
            info.append(frame.p);
            info.append("_");
            info.append(stack.size());
            info.append(" ");
        }
        System.out.println(info);
    }

    @Test
    public void t1() {
        int[] nums = {2, 3, 1, 1, 4};
        int r = 2;
        assertEquals(r, new P45().jump(nums));
    }

    @Test
    public void t2() {
        int[] nums = {1, 1, 1, 1, 1};
        int r = 4;
        assertEquals(r, new P45().jump(nums));
    }

    @Test
    public void t3() {
        int[] nums = {1, 1, 2, 1, 1};
        int r = 3;
        assertEquals(r, new P45().jump(nums));
    }

    @Test
    public void t4() {
        int[] nums = {4, 2, 7, 2, 1, 3};
        int r = 2;
        assertEquals(r, new P45().jump(nums));
    }

    @Test
    public void t5() {
        int[] nums = {0};
        int r = 0;
        assertEquals(r, new P45().jump(nums));
    }

    @Test
    public void t6() {
        int[] nums = {5, 6, 4, 4, 6, 9, 4, 4, 7, 4, 4, 8, 2, 6, 8, 1, 5, 9, 6, 5, 2, 7, 9, 7, 9, 6, 9, 4, 1, 6, 8, 8, 4, 4, 2, 0, 3, 8, 5};
        int r = 5;
        assertEquals(r, new P45().jump(nums));
    }

    @Test
    public void t7() {
        int[] nums = {8, 2, 4, 4, 4, 9, 5, 2, 5, 8, 8, 0, 8, 6, 9, 1, 1, 6, 3, 5, 1, 2, 6, 6, 0, 4, 8, 6, 0, 3, 2, 8, 7, 6, 5, 1, 7, 0, 3, 4, 8, 3, 5, 9, 0, 4, 0, 1, 0, 5, 9, 2, 0, 7, 0, 2, 1, 0, 8, 2, 5, 1, 2, 3, 9, 7, 4, 7, 0, 0, 1, 8, 5, 6, 7, 5, 1, 9, 9, 3, 5, 0, 7, 5};
        int r = 13;
        assertEquals(r, new P45().jump(nums));
    }

}
