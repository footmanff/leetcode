package com.footmanff.leetcode;

import java.util.HashSet;
import java.util.Set;

/**
 * https://leetcode.com/problems/longest-substring-without-repeating-characters/
 *
 * @author footmanff on 2020/3/30.
 */
public class P3 {

    /**
     * 暴力
     */
    public int lengthOfLongestSubstring(String str) {
        if (str == null || str.length() == 0) {
            return 0;
        }
        int length = str.length();

        Character c;
        Set<Character> set = new HashSet<>(8);

        int max = 1;

        for (int i = 0; i < length; i++) {
            for (int j = i; j < length; j++) {
                c = str.charAt(j);

                boolean added = set.add(c);

                if (!added) {
                    if (set.size() > max) {
                        max = set.size();
                        if (max >= last(i, length)) {
                            return max;
                        }
                    }
                    break;
                } else if (j == length - 1) {
                    if (set.size() > max) {
                        max = set.size();
                        if (max >= last(i, length)) {
                            return max;
                        }
                    }
                    break;
                }
            }
            set.clear();
        }
        return max;
    }

    private int last(int i, int length) {
        return length - 1 - (i + 1) + 1;
    }

    public static void main(String[] args) {
        P3 p1 = new P3();
        int a = p1.lengthOfLongestSubstring("anviaj");
        System.out.println(a);
    }

}
