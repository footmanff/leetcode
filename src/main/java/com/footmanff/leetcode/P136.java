package com.footmanff.leetcode;

import org.junit.Assert;
import org.junit.Test;

/**
 * https://leetcode-cn.com/problems/single-number/
 *
 * @author footmanff on 2020/9/4.
 */
public class P136 {

    /*
        输入: [4,1,2,1,2]
        输出: 4
        
        要求O(n)，不申请内存
     */
    public int singleNumber(int[] nums) {
        int r = 0;
        for (int num : nums) {
            r = r ^ num;
        }
        return r;
    }

    @Test
    public void t1() {
        int[] nums = {4, 1, 2, 1, 2};
        Assert.assertEquals(new P136().singleNumber(nums), 1);
    }

}
